﻿
namespace PassWordManager
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputForm));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ps_box = new System.Windows.Forms.TextBox();
            this.ps_label = new System.Windows.Forms.Label();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.titleLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(224, 150);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 38);
            this.button2.TabIndex = 11;
            this.button2.Text = "取消";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(58, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 38);
            this.button1.TabIndex = 10;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ps_box
            // 
            this.ps_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ps_box.Location = new System.Drawing.Point(97, 82);
            this.ps_box.Name = "ps_box";
            this.ps_box.Size = new System.Drawing.Size(206, 21);
            this.ps_box.TabIndex = 9;
            // 
            // ps_label
            // 
            this.ps_label.AutoSize = true;
            this.ps_label.Location = new System.Drawing.Point(38, 85);
            this.ps_label.Name = "ps_label";
            this.ps_label.Size = new System.Drawing.Size(53, 12);
            this.ps_label.TabIndex = 8;
            this.ps_label.Text = "Password";
            // 
            // titleBox
            // 
            this.titleBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.titleBox.Location = new System.Drawing.Point(97, 36);
            this.titleBox.Name = "titleBox";
            this.titleBox.Size = new System.Drawing.Size(206, 21);
            this.titleBox.TabIndex = 7;
            // 
            // titleLable
            // 
            this.titleLable.AutoSize = true;
            this.titleLable.Location = new System.Drawing.Point(56, 39);
            this.titleLable.Name = "titleLable";
            this.titleLable.Size = new System.Drawing.Size(35, 12);
            this.titleLable.TabIndex = 6;
            this.titleLable.Text = "Title";
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(213)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(381, 234);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ps_box);
            this.Controls.Add(this.ps_label);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.titleLable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InputForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InputForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ps_box;
        private System.Windows.Forms.Label ps_label;
        private System.Windows.Forms.TextBox titleBox;
        private System.Windows.Forms.Label titleLable;
    }
}