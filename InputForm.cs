﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassWordManager
{
    public partial class InputForm : Form
    {

        private DoWork doWork;
        private int? lineIndex;
        public InputForm(string title,string password,DoWork doWork,int? lineIndex)
        {
            InitializeComponent();
            this.titleBox.Text = title;
            this.ps_box.Text = password;
            this.doWork = doWork;
            this.lineIndex = lineIndex;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            doWork(this.titleBox.Text, this.ps_box.Text, lineIndex);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
