# DesPasswordManager

#### 介绍
一个基于winform+des对称加密算法的密码管理小工具

#### 软件架构
主界面如下
![输入图片说明](https://foruda.gitee.com/images/1661172343183319160/屏幕截图.png "屏幕截图.png")


#### 安装教程

直接双击PassWordManager.exe即可打开。

#### 使用说明

1.  首先填写8位以上的混淆码，越长越复杂则越难被别人破解。这个是我们必须要记住的，解密的时候要用到。
![输入图片说明](https://foruda.gitee.com/images/1661172523086383746/屏幕截图.png "屏幕截图.png")
2.  增加密码
![输入图片说明](https://foruda.gitee.com/images/1661172568455338792/屏幕截图.png "屏幕截图.png")
3.  填写并添加
![输入图片说明](https://foruda.gitee.com/images/1661172724535923239/屏幕截图.png "屏幕截图.png")
4.  生成结果
![输入图片说明](https://foruda.gitee.com/images/1661172893613935585/屏幕截图.png "屏幕截图.png")
![输入图片说明](https://foruda.gitee.com/images/1661172982878474160/屏幕截图.png "屏幕截图.png")
