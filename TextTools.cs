﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PassWordManager
{
    public class TextFileUtil
    {
        private static string path=".\\path.text";
        private TextFileUtil()
        {
        }
        public static LinkedList<string> getTextLines(string textFilePath)
        {
            LinkedList<string> lines = new LinkedList<string>();
            if (!File.Exists(path))
            {
                File.Create(path);
                return lines;
            }
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.AddLast(line);
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return lines;
        }
        public static LinkedList<string> getTextLines()
        {
            return getTextLines(path);
        }

        public static void saveTextLines(LinkedList<string> lines)
        {
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (string s in lines)
                {
                    sw.WriteLine(s);
                }
            }
        }
    }
}
